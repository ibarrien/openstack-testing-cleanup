# Openstack machines cleanup

Docker image for cleaning up machines generated during <https://gitlab.cern.ch/linuxsupport/koji-image-build> and <https://gitlab.cern.ch/imageci> and old CentOS test images.

Marking machine's metadata key `centos_test_cleanup` as `false` will avoid the cleaning up. This is useful for debugging why a test failed.

## Behaviour

This image will run in a scheduled job that performs the following actions:

1. Checks for any virtual or physical machines running on a given Openstack tenant (`--tenant_name`)
1. For each node marked with `centos_test_cleanup` metadata property
    1. For all retrieved servers, it deletes any failed machine, no matter how or from where it spawned
    1. If marked with `puppet_managed==true` it will be processed as a puppet machine and will be deleted using `ai-kill` to remove Foreman entries
    1. If marked with `puppet_managed==false` it will be processed as a non-puppet machine and will be deleted using `openstack server delete`
1. According to metadata options checks for machines that ran longer than `ACTIVE_HOURS_THRESHOLD`.
